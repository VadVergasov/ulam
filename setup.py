from setuptools import setup, find_packages

setup(name='ulam_spiral',
      version='1.2',
      description='Ulam spiral',
      platforms='any',
      url='https://gitlab.com/VadVergasov/ulam',
      long_description=open('README.rst').read(),
      author='VadVergasov',
      author_email='vadim.vergasov2003@gmail.com',
      license='MIT',
      packages=['ulam'],
      keywords='ulam spiral numbers',
      install_requires=['pillow'])